void main(List<String> arguments) {
  final result = sum('1234', '5678');
  print('Sum of two params : $result');
}

String sum(String paramA, String paramB) {
  List<int> digitsParamA = convertStringToNum(paramA);
  List<int> digitsParamB = convertStringToNum(paramB);
  final sumDigitsA = sumAllDigits(digitsParamA, 'A');
  final sumDigitsB = sumAllDigits(digitsParamB, 'B');
  String finalResult = (sumDigitsA + sumDigitsB).toString();
  return finalResult;
}

int sumAllDigits(List<int> digits, String param) {
  int sum = 0, step = 0;
  print('Param $param:');
  for (var i = digits.length - 1; i >= 0; i--) {
    if (i == digits.length - 1) {
      int prevNum = digits[i];
      int nextNum = digits[i - 1];
      sum = prevNum + nextNum;
      step++;
      i = digits.length - 2;
      print('Step $step : sum = $prevNum + $nextNum = $sum');
    } else {
      int oldSum = sum;
      sum += digits[i];
      step++;
      print('Step $step : sum = $oldSum + ${digits[i]} = $sum');
    }
  }
  return sum;
}

List<int> convertStringToNum(String digits) {
  List<int> result = [];
  for (var i = 0; i < digits.length; i++) {
    int digit = int.parse(digits[i]);
    result.add(digit);
  }
  return result;
}
